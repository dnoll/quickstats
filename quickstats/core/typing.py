import numbers
from typing import Union
from numpy.typing import ArrayLike

__all__ = ["Numeric", "Scalar", "Real", "ArrayLike", "NOTSET"]

Numeric = Union[int, float]

Scalar = Numeric

Real = numbers.Real

# sentinel object to indicate if a parameter is supplied
class _NOTSET_TYPE:
    pass

NOTSET = _NOTSET_TYPE()