from typing import Dict, Optional, Union, List, Tuple, Callable
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import matplotlib.colorbar as mcolorbar

from quickstats.plots import AbstractPlot
from quickstats.plots.template import format_axis_ticks
from quickstats.maths.interpolation import interpolate_2d
from quickstats.utils.common_utils import combine_dict

class General2DPlot(AbstractPlot):
    
    STYLES = {
        'pcolormesh': {
            'shading': 'auto',
            'rasterized': True
        },
        'colorbar': {
            'pad': 0.02,
        },
        'contour': {
            'linestyles': 'solid',
            'linewidths': 2
        },
        'contourf': {
        },
        'scatter': {
            'c': 'hh:darkpink',
            'marker': 'o',
            's': 40,
            'edgecolors': 'hh:darkblue',
            'alpha': 0.7,
            'linewidth': 1,
        },
        'legend': {
            'handletextpad': 0.,
        },
        'clabel': {
            'inline': True,
            'fontsize': 10
        }
    }
    
    CONFIG = {
        'interpolation': 'cubic',
        'num_grid_points': 500
    }    
    
    def __init__(self, data:pd.DataFrame,
                 styles:Optional[Union[Dict, str]]=None,
                 analysis_label_options:Optional[Dict]=None,
                 config:Optional[Dict]=None):
        
        self.data = data
        
        super().__init__(styles=styles,
                         analysis_label_options=analysis_label_options,
                         config=config)
    
    def draw(self, xattrib:str, yattrib:str, zattrib:str,
             xlabel:Optional[str]=None, ylabel:Optional[str]=None,
             zlabel:Optional[str]=None, title:Optional[str]=None,
             ymin:Optional[float]=None, ymax:Optional[float]=None,
             xmin:Optional[float]=None, xmax:Optional[float]=None,
             zmin:Optional[float]=None, zmax:Optional[float]=None,
             logx:bool=False, logy:bool=False, logz:bool=False,
             norm:Optional=None, cmap:str='GnBu', 
             draw_colormesh:bool=True, draw_contour:bool=False,
             draw_contourf:bool=False, draw_scatter:bool=False,
             draw_clabel:bool=True, draw_colorbar:bool=True,
             clabel_fmt:Union[str, Dict]='%1.3f',
             clabel_manual:Optional[Dict] = None,
             contour_levels:Optional[Union[float, List[float]]]=None,
             transform:Optional[Callable]=None, ax=None):

        if (zmin is not None) or (zmax is not None):
            if norm is not None:
                raise ValueError(f'Cannot specify both (zmin, zmax) and norm.')
            if logz:
                norm = mcolors.LogNorm(vmin=zmin, vmax=zmax)
            else:
                norm = mcolors.Normalize(vmin=zmin, vmax=zmax)
                
        if ax is None:
            ax = self.draw_frame(logx=logx, logy=logy)
        
        data = self.data
        x, y, z = data[xattrib], data[yattrib], data[zattrib]
        
        if transform:
            z = transform(z)
        if norm is None:
            norm = mcolors.Normalize(vmin=np.min(z), vmax=np.max(z))
        
        if draw_colormesh or draw_contour or draw_contourf:
            interp_method = self.config['interpolation']
            n = self.config['num_grid_points']
            X, Y, Z = interpolate_2d(x, y, z, method=interp_method, n=n)
            self.Z = Z

        handles = {}
        if draw_colormesh:
            pcm_styles = combine_dict(self.styles['pcolormesh'])
            if cmap is not None:
                pcm_styles['cmap'] = cmap
            if norm is not None:
                pcm_styles.pop('vmin', None)
                pcm_styles.pop('vmax', None)
            pcm = ax.pcolormesh(X, Y, Z, norm=norm, **pcm_styles)
            handles['pcm'] = pcm
            
        if draw_contour:
            contour_styles = combine_dict(self.styles['contour'])
            if 'colors' not in contour_styles:
                contour_styles['cmap'] = cmap
            contour = ax.contour(X, Y, Z, levels=contour_levels,
                                 norm=norm, **contour_styles)
            handles['contour'] = contour
            
            if draw_clabel:
                clabel_styles = combine_dict(self.styles['clabel'])
                clabel_styles['fmt'] = clabel_fmt
                clabel_styles['manual'] = clabel_manual
                clabel = ax.clabel(contour, **clabel_styles)
                handles['clabel'] = clabel
                
        if draw_contourf:
            contourf_styles = combine_dict(self.styles['contourf'])
            if 'colors' not in contourf_styles:
                contourf_styles['cmap'] = cmap
            contourf = ax.contourf(X, Y, Z, levels=contour_levels,
                                   norm=norm, **contourf_styles)
            handles['contourf'] = contourf
            
        if draw_scatter:
            scatter = ax.scatter(x, y, **self.styles['scatter'])
            handles['scatter'] = scatter

        if draw_colorbar:
            if 'pcm' in handles:
                mappable = pcm
            elif 'contourf' in handles:
                mappable = contourf
            elif 'contour' in handles:
                mappable = contour
            else:
                mappable = None
            if mappable is not None:
                cbar = plt.colorbar(mappable, ax=ax, **self.styles['colorbar'])
                if zlabel:
                    cbar.set_label(zlabel, **self.styles['colorbar_label'])
                format_axis_ticks(cbar.ax, **self.styles['colorbar_axis'])
                handles['cbar'] = cbar

        self.update_legend_handles(handles, raw=True)
             
        self.draw_axis_components(ax, xlabel=xlabel, ylabel=ylabel,
                                  title=title)
        self.set_axis_range(ax, xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax)

        self.finalize(ax)

        self.draw_legend(ax)
        
        return ax