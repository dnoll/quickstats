from typing import List, Dict, Optional, Union

import numpy as np

from matplotlib.colors import ListedColormap, LinearSegmentedColormap

import quickstats
from quickstats import DescriptiveEnum
from quickstats.utils.common_utils import combine_dict

from .colors import (
    register_colors, register_cmaps,
    plot_color_gradients, get_cmap,
    get_rgba, get_cmap_rgba,
    get_color_cycle
)

__all__ = ["ErrorDisplayFormat", "PlotFormat",
           "set_attrib", "reload_styles", "use_style",
           "register_colors", "register_cmaps",
           "plot_color_gradients", "get_cmap",
           "get_rgba", "get_cmap_rgba", "get_color_cycle"]

class ErrorDisplayFormat(DescriptiveEnum):
    ERRORBAR = (0, "Error bar", "errorbar")
    FILL     = (1, "Fill interpolated error range across bins", "fill_between")
    SHADE    = (2, "Shade error range in each bin", "bar")
    
    def __new__(cls, value:int, description:str="", mpl_method:str=""):
        obj = object.__new__(cls)
        obj._value_ = value
        obj.description = description
        obj.mpl_method = mpl_method
        return obj
    
class PlotFormat(DescriptiveEnum):
    ERRORBAR = (0, "Error bar", "errorbar")
    HIST     = (1, "Histogram", "hist")
                
    def __new__(cls, value:int, description:str="", mpl_method:str=""):
        obj = object.__new__(cls)
        obj._value_ = value
        obj.description = description
        obj.mpl_method = mpl_method
        return obj

def set_attrib(obj, **kwargs):
    for key, value in kwargs.items():
        target = obj
        if '.' in key:
            tokens = key.split('.')
            if len(tokens) != 2:
                raise ValueError('maximum of 1 subfield is allowed but {} is given'.format(len(tokens)-1))
            field, key = tokens[0], tokens[1]
            method_name = 'Get' + field
            if hasattr(obj, 'Get' + field):
                target = getattr(obj, method_name)()
            else:
                raise ValueError('{} object does not contain the method {}'.format(type(target), method_name)) 
        method_name = 'Set' + key
        if hasattr(target, 'Set' + key):
            method_name = 'Set' + key
        elif hasattr(target, key):
            method_name = key
        else:
            raise ValueError('{} object does not contain the method {}'.format(type(target), method_name))         
        if value is None:
            getattr(target, method_name)()
        elif isinstance(value, (list, tuple)):
            getattr(target, method_name)(*value)
        else:
            getattr(target, method_name)(value)
    return obj

def reload_styles():
    from matplotlib import style
    style.core.USER_LIBRARY_PATHS.append(quickstats.stylesheet_path)
    style.core.reload_library()

def use_style(name:str='quick_default'):
    from matplotlib import style
    style.use(name)