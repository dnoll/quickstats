import quickstats

from .core import *

quickstats.methods._require_module("kerberos", is_kerberos_installed)