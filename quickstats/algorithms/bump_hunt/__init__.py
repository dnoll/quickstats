from quickstats.core.methods import _require_module_version
_require_module_version('python', (3, 8, 0))
_require_module_version('pydantic', (2, 0, 0))

from .bump_hunt_1d import BumpHunt1D