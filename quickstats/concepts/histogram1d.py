from typing import Optional, Union, Tuple, Any

import numpy as np

from quickstats import stdout
from quickstats.core.typing import ArrayLike, Real
from quickstats.maths.statistics import BinErrorMode, poisson_interval, histogram
from quickstats.maths.numerics import all_integers, safe_div, is_integer
from .binning import Binning

BinErrorType = Optional[Tuple[np.ndarray, np.ndarray]]

class Histogram1D:
    """
    A class for defining a 1D histogram.

    Parameters
    ----------
    bin_content : np.ndarray
        The bin content of the histogram.
    bin_edges : np.ndarray
        The bin edges of the histogram.
    bin_errors : ArrayLike, optional
        The bin errors of the histogram. If None and a Poisson error mode
        is used, the bin errors will be automatically calculated.
    error_mode : BinErrorMode or str, default = "auto"
        The method with which the bin errors are evaluated. It can
        be "sumw2" (symmetric error from Wald approximation), "poisson"
        (Poisson interval at one sigma), "auto" (deduce automatically from
        bin_content, use "poisson" if bin content is integer type or float
        type with zero remainders, and "sumw2" otherwise). Note that this
        method only indicates the method with which the bin_errors are
        calculated at initialization. Subsequent evaluation could differ
        depending on the operations (e.g. summation or division of histograms,
        and scaling of histograms).

    Attributes
    ----------
    bin_content : np.ndarray
        The bin content of the histogram.
    bin_errors : (np.ndarray, np.ndarray)
        The bin errors of the histogram.
    bin_edges : np.ndarray
        The bin edges of the histogram.
    bin_centers : np.ndarray
        The bin centers of the histogram.
    bin_widths : np.ndarray
        The widths of the bins.
    nbins : int
        The number of bins.
    error_mode : BinErrorMode
        The current error mode of the histogram.
    """

    def __init__(self, bin_content: np.ndarray,
                 bin_edges: np.ndarray,
                 bin_errors: Optional[ArrayLike] = None,
                 error_mode: Union[BinErrorMode, str] = "auto") -> None:
        """
        Initialize the Histogram1D object by setting the bin content, edges, errors, and error mode.
        """
        self.set_data(bin_content=bin_content,
                      bin_edges=bin_edges,
                      bin_errors=bin_errors,
                      error_mode=error_mode)

    def __add__(self, other: "Histogram1D") -> "Histogram1D":
        """Perform addition between two histograms."""
        return self._operate('add', other)

    def __iadd__(self, other: "Histogram1D") -> "Histogram1D":
        """Perform in-place addition between two histograms."""
        return self._ioperate('add', other)

    def __sub__(self, other: "Histogram1D") -> "Histogram1D":
        """Perform subtraction between two histograms."""
        return self._operate('sub', other)

    def __isub__(self, other: "Histogram1D") -> "Histogram1D":
        """Perform in-place subtraction between two histograms."""
        return self._ioperate('sub', other)

    def __truediv__(self, other: Union[Real, ArrayLike, "Histogram1D"]) -> "Histogram1D":
        """Perform division between a histogram and either a scalar or another histogram."""
        instance = self._operate('div', other)
        # Ensure that bin content is treated as weighted after division
        instance._bin_content = instance._bin_content.astype(float)
        return instance

    def __itruediv__(self, other: Union[Real, ArrayLike, "Histogram1D"]) -> "Histogram1D":
        """Perform in-place division between a histogram and either a scalar or another histogram."""
        return self._ioperate('div', other)

    def __mul__(self, other: Union[Real, ArrayLike]) -> "Histogram1D":
        """Perform multiplication of a histogram by a scalar."""
        return self._operate('scale', other)

    def __imul__(self, other: Union[Real, ArrayLike]) -> "Histogram1D":
        """Perform in-place multiplication of a histogram by a scalar."""
        return self._ioperate('scale', other)

    def __rmul__(self, other: Union[Real, ArrayLike]) -> "Histogram1D":
        """Perform right multiplication of a histogram by a scalar."""
        return self._operate('scale', other)

    def _operate(self, method: str, other: Any) -> "Histogram1D":
        """Perform a binary operation (add, sub, div, scale) on the histogram."""
        if not hasattr(self, f'_{method}'):
            raise ValueError(f'no operation named "_{method}"')
        fn = getattr(self, f'_{method}')
        bin_content, bin_errors = fn(other)
        if isinstance(other, Histogram1D):
            error_mode = self._resolve_error_mode(bin_content, other._error_mode)
        else:
            error_mode = self._error_mode
        # Avoid copying bin_edges
        bin_edges = self._binning._bin_edges
        return type(self)(bin_content=bin_content, bin_edges=bin_edges,
                          bin_errors=bin_errors, error_mode=error_mode)

    def _ioperate(self, method: str, other: Any) -> "Histogram1D":
        """Perform an in-place binary operation (add, sub, div, scale) on the histogram."""
        if not hasattr(self, f'_{method}'):
            raise ValueError(f'no operation named "_{method}"')
        fn = getattr(self, f'_{method}')
        bin_content, bin_errors = fn(other)
        if isinstance(other, Histogram1D):
            self._error_mode = self._resolve_error_mode(bin_content, other._error_mode)
        self._bin_content = bin_content
        self._bin_errors = bin_errors
        return self

    def _validate_other(self, other: "Histogram1D") -> None:
        """Ensure the other histogram is of the same binning and valid for operations."""
        if not isinstance(other, Histogram1D):
            raise ValueError(f'operation only allowed with another Histogram1D object')
        if self.binning != other.binning:
            raise ValueError(f'operations not allowed between histograms with different binnings')

    def _resolve_error_mode(self, bin_content: np.ndarray, other_mode: BinErrorMode) -> BinErrorMode:
        """
        Resolve the error mode based on bin content and other histogram's error mode.
        Prefer Poisson errors if possible.
        """
        if ((bin_content.dtype == 'int64') or
            (self._error_mode == BinErrorMode.POISSON) or
            (other_mode == BinErrorMode.POISSON)):
            return BinErrorMode.POISSON
        return BinErrorMode.SUMW2

    def _scale(self, val: Union[Real, ArrayLike]) -> Tuple[np.ndarray, BinErrorType]:
        """
        Scale the histogram bin content and errors by a scalar value or by an array of
        values with size matching the number of bins.
        """
        val = np.asarray(val)
        is_weighted = self.is_weighted()
        if (not is_weighted) and all_integers(val):
            val = val.astype(int)
            if not np.all(val >= 0.):
                stdout.warning('scaling unweighted histogram by negative value(s) will make it weighted'
                               ' and force usage of sumw2 errors')
                val = val.astype(float)
        else:
            val = val.astype(float)
        if val.ndim == 0:
            val = val[()]
        elif val.ndim == 1:
            if val.size != self.nbins:
                raise ValueError(f'size of array ({val.size}) does not match number '
                                 f'of bins ({self.nbins}) of histogram')
        else:
            raise ValueError(f'cannot scale a histogram with a value of dimension {val.ndim}')
        
        bin_content = self._bin_content * val

        if self._bin_errors is None:
            return bin_content, None

        if bin_content.dtype == 'int64':
            bin_errors = poisson_interval(bin_content)
        else:
            errlo, errhi = self._bin_errors
            bin_errors = (val * errlo, val * errhi)

        return bin_content, bin_errors

    def _add(self, other: "Histogram1D", neg: bool = False) -> Tuple[np.ndarray, BinErrorType]:
        """Add (or subtract if `neg` is True) two histograms."""
        self._validate_other(other)
        if neg:
            bin_content = self._bin_content - other._bin_content
        else:
            bin_content = self._bin_content + other._bin_content

        if (self._bin_errors is None) and (other._bin_errors is None):
            return bin_content, None

        if (self._bin_errors is not None) and (other._bin_errors is not None):
            use_poisson = False
            if bin_content.dtype == 'int64':
                if np.all(bin_content >= 0):
                    use_poisson = True
                else:
                    stdout.warning('Histogram has negative bin content - force usage of sumw2 errors')
            if use_poisson:
                bin_errors = poisson_interval(bin_content)
            else:
                errlo = np.sqrt(self._bin_errors[0] ** 2 + other._bin_errors[0] ** 2)
                errhi = np.sqrt(self._bin_errors[1] ** 2 + other._bin_errors[1] ** 2)
                bin_errors = (errlo, errhi)
        else:
            if self._bin_errors is None:
                bin_errors = other.bin_errors
            else:
                bin_errors = self.bin_errors

        return bin_content, bin_errors

    def _sub(self, other: "Histogram1D") -> Tuple[np.ndarray, BinErrorType]:
        """Subtract another histogram from the current histogram."""
        return self._add(other, neg=True)

    def _div(self, other: Any) -> Tuple[np.ndarray, ArrayLike, BinErrorType]:
        """Divide the current histogram by either a scalar or another histogram."""
        if not isinstance(other, Histogram1D):
            scale = 1. / other
            return self._scale(scale)
        self._validate_other(other)
        bin_content = safe_div(self._bin_content, other._bin_content, True)
        # Enforce sumw2 error by making bin content weighted
        if bin_content.dtype == 'int64':
            bin_content = bin_content.astype(float)

        if (self._bin_errors is None) and (other._bin_errors is None):
            return bin_content, None

        # NB: can be jitted / vectorized
        def get_err(b1, b2, e1, e2):
            b2_sq = b2 * b2
            err2 = safe_div(e1 * e1 * b2_sq + e2 * e2 * b1 * b1, b2_sq * b2_sq, True)
            return np.sqrt(err2)

        err1 = self._bin_errors or (np.zeros(self.nbins), np.zeros(self.nbins))
        err2 = other._bin_errors or (np.zeros(other.nbins), np.zeros(other.nbins))

        errlo = get_err(self._bin_content, other._bin_content, err1[0], err2[0])
        errhi = get_err(self._bin_content, other._bin_content, err1[1], err2[1])
        bin_errors = (errlo, errhi)

        return bin_content, bin_errors

    @staticmethod
    def _regularize_errors(bin_content: np.ndarray,
                           bin_errors: Optional[ArrayLike] = None) -> BinErrorType:
        if bin_errors is None:
            return None
        size = bin_content.size
        ndim = np.ndim(bin_errors)
        # make sure bin_errors is a 2-tuple
        if ndim == 0:
            bin_errors = (bin_errors, bin_errors)
        elif ndim == 1:
            if isinstance(bin_errors, tuple):
                if len(bin_errors) != 2:
                    raise ValueError(f'`bin_errors` should have size 2 when given as a tuple.')
            else:
                bin_errors = (bin_errors, bin_errors)
        elif ndim == 2:
            bin_errors = np.asarray(bin_errors)
            if bin_errors.shape[0] != 2:
                raise ValueError(f'`bin_errors` should have shape (2, N) when given as an array.')
            bin_errors = (bin_errors[0], bin_errors[1])
        else:
            raise RuntimeError(f'`bin_errors` should have dimension less than 3.')
        assert isinstance(bin_errors, tuple) and (len(bin_errors) == 2)
        result = []
        for errors in bin_errors:
            if isinstance(errors, Real):
                errors = np.full(size, errors, dtype=float)
            else:
                errors = np.array(errors)
            result.append(errors)
        if bin_errors[0].shape != bin_errors[1].shape:
            raise ValueError(f'upper and lower errors must have the same shape.')
        if bin_errors[0].shape != bin_content.shape:
            raise ValueError(f'bin content and bin errors (upper or lower) must have the same shape.')
        return bin_errors[0], bin_errors[1]

    def set_data(self, bin_content: np.ndarray,
                 bin_edges: np.ndarray,
                 bin_errors: Optional[ArrayLike] = None,
                 error_mode: Union[BinErrorMode, str] = "auto") -> None:
        """
        Set the histogram's data including bin content, bin edges, bin errors, and error mode.

        Parameters
        ----------
        bin_content : np.ndarray
            The bin content for the histogram.
        bin_edges : np.ndarray
            The bin edges for the histogram.
        bin_errors : ArrayLike, optional
            The bin errors. If None, errors will be calculated automatically.
        error_mode : BinErrorMode or str, default = "auto"
            The error mode to use.
        """
        bin_content = np.array(bin_content)
        if bin_content.ndim != 1:
            raise ValueError(f'`bin_content` must be a 1D array.')
        bin_edges = np.array(bin_edges)
        if bin_edges.ndim != 1:
            raise ValueError(f'`bin_edges` must be a 1D array.')
        if bin_content.size != (bin_edges.size - 1):
            raise RuntimeError(f'number of bins from bin content (= {bin_content.size}) '
                               f'is different from that from bin edges (= {bin_edges.size - 1})')

        binning = Binning(bins=bin_edges)
        error_mode = BinErrorMode.parse(error_mode)

        if error_mode == BinErrorMode.AUTO:
            error_mode = BinErrorMode.POISSON if all_integers(bin_content) else BinErrorMode.SUMW2

        # Coerce bin content type based on error mode
        if error_mode == BinErrorMode.POISSON:
            if (bin_content.dtype != 'int64'):
                # cast to int because dtype is used to check whether histogram is weighted
                if all_integers(bin_content):
                    bin_content = bin_content.astype(int)
                else:
                    bin_content = bin_content.astype(float)
        else:
            bin_content = bin_content.astype(float)

        bin_errors = self._regularize_errors(bin_content, bin_errors)
        if (error_mode == BinErrorMode.POISSON) and (bin_errors is None):
            bin_errors = poisson_interval(bin_content)
        elif (error_mode == BinErrorMode.SUMW2) and bin_errors is not None:
            if not np.allclose(bin_errors[0], bin_errors[1]):
                raise ValueError('the given bin errors are not symmetric although the error mode is sumw2')

        self._bin_content = bin_content
        self._binning = binning
        self._bin_errors = bin_errors
        self._error_mode = error_mode

    @property
    def bin_content(self) -> np.ndarray:
        """Returns a copy of the bin content."""
        return self._bin_content.copy()

    @property
    def binning(self) -> Binning:
        """Returns the binning information."""
        return self._binning

    @property
    def bin_edges(self) -> np.ndarray:
        """Returns the edges of the bins."""
        return self._binning.bin_edges

    @property
    def bin_centers(self) -> np.ndarray:
        """Returns the centers of the bins."""
        return self._binning.bin_centers

    @property
    def bin_widths(self) -> np.ndarray:
        """Returns the widths of the bins."""
        return self._binning.bin_widths

    @property
    def nbins(self) -> int:
        """Returns the number of bins."""
        return self._binning.nbins

    @property
    def bin_range(self) -> Tuple[float, float]:
        """Returns the bin range as a tuple (low, high)."""
        return self._binning.bin_range

    @property
    def uniform_binning(self) -> bool:
        """Check if binning is uniform."""
        return self._binning.is_uniform

    @property
    def bin_errors(self) -> BinErrorType:
        """Returns the bin errors."""
        if self._bin_errors is None:
            return None
        errlo = self._bin_errors[0].copy()
        errhi = self._bin_errors[1].copy()
        return errlo, errhi

    @property
    def bin_errlo(self) -> Optional[np.ndarray]:
        """Returns the lower bin errors."""
        if self._bin_errors is None:
            return None
        return self._bin_errors[0].copy()

    @property
    def bin_errhi(self) -> Optional[np.ndarray]:
        """Returns the upper bin errors."""
        if self._bin_errors is None:
            return None
        return self._bin_errors[1].copy()

    @property
    def error_mode(self) -> BinErrorMode:
        """Returns the current error mode."""
        return self._error_mode

    @classmethod
    def create(cls, x:np.ndarray, weights:Optional[np.ndarray]=None,
               bins:Union[int, ArrayLike]=10,
               bin_range:Optional[ArrayLike]=None,
               underflow:bool=False,
               overflow:bool=False,
               divide_bin_width:bool=False,
               normalize:bool=True,
               clip_weight:bool=False,
               evaluate_error:bool=False,
               error_mode:Union[BinErrorMode, str]="auto",
               **kwargs):
        """
            Create histogram from unbinned data.
            
            Arguments:
            -------------------------------------------------------------------------------
            x: ndarray
                Input data array from which the histogram is computed.
            weights: (optional) ndarray
                Array of weights with same shape as input data. If not given, the
                input data is assumed to have unit weights.
            bins: (optional) int or array of scalars, default = 10
                If integer, it defines the number of equal-width bins in the
                given range.
                If an array, it defines a monotonically increasing array of bin edges,
                including the rightmost edge.
           bin_range: (optional) array of the form (float, float)
               The lower and upper range of the bins.  If not provided, range is simply 
               ``(x.min(), x.max())``.  Values outside the range are ignored.
           underflow: bool, default = False
               Include undeflow data in the first bin.
           overflow: bool, default = False
               Include overflow data in the last bin.
           divide_bin_width: bool, default = False
               Divide each bin by the bin width.           
           normalize: bool, default = True
               Normalize the sum of weights to one. Weights outside the bin range will
               not be counted if ``clip_weight`` is set to false, so the sum of bin
               content could be less than one.
           clip_weight: bool, default = False
               Ignore data outside given range when evaluating total weight
               used in normalization.
           evaluate_error: bool, default = True
               Evaluate the error of the bin contents using the given error option.
           error_mode: BinErrorMode or str, default = "auto"
               How to evaluate bin errors. If "sumw2", symmetric errors from the Wald
               approximation is used (square root of sum of squares of weights). If
               "poisson", asymmetric errors from Poisson interval at one sigma is
               used. If "auto", it will use sumw2 error if data has unit weights,
               else Poisson error will be used.
        """
        bin_content, bin_edges, bin_errors = histogram(x=x,
                                                       weights=weights,
                                                       bins=bins,
                                                       bin_range=bin_range,
                                                       underflow=underflow,
                                                       overflow=overflow,
                                                       divide_bin_width=divide_bin_width,
                                                       normalize=normalize,
                                                       clip_weight=clip_weight,
                                                       evaluate_error=evaluate_error,
                                                       error_mode=error_mode)
        instance = cls(bin_content=bin_content,
                       bin_edges=bin_edges,
                       bin_errors=bin_errors,
                       error_mode=error_mode)
        return instance

    def is_weighted(self) -> bool:
        """Check if the histogram is weighted (i.e., non-integer bin content)."""
        return self._bin_content.dtype != 'int64'

    def scale(self, val: Union[Real, ArrayLike], inplace: bool = False) -> "Histogram1D":
        """Scale the histogram by a scalar value."""
        if inplace:
            return self._ioperate('scale', val)
        return self._operate('scale', val)

    def sum(self) -> float:
        return self._bin_content.sum()

    def integral(self) -> float:
        return (self.bin_widths * self._bin_content).sum()

    def normalize(self, density: bool = False, inplace: bool = False) -> "Histogram1D":
        norm_factor = self.sum()
        if density:
            norm_factor *= self.bin_widths
        if inplace:
            return self._ioperate('div', norm_factor)
        return self._operate('div', norm_factor)
        